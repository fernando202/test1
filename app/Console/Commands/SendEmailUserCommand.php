<?php

namespace App\Console\Commands;

use App\Models\Newsletter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendEmailUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-email-user-command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Log::info('Start:: command:send_email_user_command');
        try {

            $usersGet = User::where('',1);
            Log::info('END:: command:send_email_user_command');

        }catch (\Exception$e) {
            Log::error($e->getMessage());
            Log::info('END AFTER EXCEPTIONS :: send_email_user_command');
        }
        return;
    }
}
